﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson.Points
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new Point
            {
                x = 10,
                y = 20
            };
            //p.Print();
            //Console.WriteLine();
            Print(p);

            var ci = new Circle
            {
                x = 10,
                y = 20,
                R = -10
            };

            //ci.Print();
            //Console.WriteLine();
            Print(ci);

            var cy = new Cylinder
            {
                x = 10,
                y = 20,
                R = -10,
                H = -6
            };

            //cy.Print();
            //Console.WriteLine();
            Print(cy);

            Console.ReadLine();
        }

        private static void Print(Point item)
        {
            string typeName = item.GetType().Name;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write($"{typeName}:\t");
            Console.ResetColor();
            item.Print();
            Console.WriteLine();
        }
    }
}
