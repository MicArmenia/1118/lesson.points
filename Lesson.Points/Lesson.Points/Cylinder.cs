﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson.Points
{
    class Cylinder : Circle
    {
        public Cylinder()
        {
            h = 5;
        }

        private int h;
        public int H
        {
            get { return h; }
            set
            {
                if (value < 1 || value > 100)
                    h = 5;
                else
                    h = value;
            }
        }

        public override void Print()
        {
            base.Print();
            Console.Write($", {H}");
        }
    }
}
