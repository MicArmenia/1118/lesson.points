﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson.Points
{
    class Point
    {
        public int x;
        public int y;

        public virtual void Print()
        {
            Console.Write($"({x},{y})");
        }
    }
}