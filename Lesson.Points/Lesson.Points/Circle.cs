﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson.Points
{
    class Circle : Point
    {
        public Circle()
        {
            r = 5;
        }

        private int r;
        public int R
        {
            get { return r; }
            set
            {
                if (value < 1 || value > 100)
                    r = 5;
                else
                    r = value;
            }
        }

        public override void Print()
        {
            base.Print();
            Console.Write($", {R}");
        }
    }
}
